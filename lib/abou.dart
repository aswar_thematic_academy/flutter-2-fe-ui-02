import 'package:flutter/material.dart';

class AboutPage extends StatelessWidget {
  AboutPage({this.judul, this.kata});
  String kata;
  String judul;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("About page"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(this.judul),
            Text(this.kata),
            FlatButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text("Kembali kan"),
              color: Colors.red,
            )
          ],
        ),
      ),
    );
  }
}

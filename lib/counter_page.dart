import 'package:flutter/material.dart';

class CounterPage extends StatefulWidget {
  _CounterPage createState() => _CounterPage();
}

class _CounterPage extends State<CounterPage> {
  int angka = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('counte page'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              angka.toString(),
              style: TextStyle(fontSize: 24),
            ),
            SizedBox(
              height: 40,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                FloatingActionButton(
                  heroTag: "Btn1",
                  onPressed: () {
                    setState(() {
                      angka++;
                    });
                  },
                  child: Icon(Icons.add),
                ),
                SizedBox(
                  width: 60,
                ),
                FloatingActionButton(
                  heroTag: "Btn2",
                  onPressed: () {
                    setState(() {
                      angka--;
                    });
                  },
                  child: Icon(Icons.remove),
                  backgroundColor: Colors.red,
                ),
              ],
            ),
            SizedBox(
              height: 60,
            ),
            FlatButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text("Kembali"),
              color: Colors.red,
            ),
          ],
        ),
      ),
    );
  }
}

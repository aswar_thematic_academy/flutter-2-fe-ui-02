import 'package:flutter/material.dart';
import 'package:latihan1_statetfull_widget/text_form.dart';

class ScoreTeam extends StatelessWidget {
  ScoreTeam({this.tim, this.lawan});
  String tim;
  String lawan;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Hasil Pertandingan"),
        ),
        body: Container(
          margin: EdgeInsets.only(left: 10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "HASIL SEMENTARA",
                    style: TextStyle(
                      fontSize: 30.0,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Montserrat',
                    ),
                  ),
                  SizedBox(
                    height: 120,
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    tim,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  ),
                  SizedBox(
                    width: 80,
                  ),
                  Text(
                    lawan,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  ),
                ],
              ),
              Container(
                child: Column(
                  //mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    NumberScreen(),
                  ],
                ),
              )
            ],
          ),
        ));
  }
}

class NumberScreen extends StatefulWidget {
  @override
  _NumberScreenState createState() => _NumberScreenState();
}

class _NumberScreenState extends State<NumberScreen> {
  int score_tim = 0;
  int score_lawan = 0;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            children: [
              Row(
                children: [
                  Text(
                    score_tim.toString(),
                    style: TextStyle(fontSize: 100),
                  ),
                  SizedBox(
                    width: 120,
                  ),
                  Text(
                    score_lawan.toString(),
                    style: TextStyle(fontSize: 100),
                  ),
                ],
              ),

              //TOMBOL

              Column(
                children: [
                  //TOMBOL TIM
                  Row(
                    children: [
                      RaisedButton(
                        onPressed: () {
                          setState(() {
                            score_tim++;
                          });
                        },
                        child: Icon(Icons.add),
                        color: Colors.green,
                      ),
                      RaisedButton(
                        onPressed: () {
                          setState(() {
                            score_tim--;
                          });
                        },
                        child: Icon(Icons.remove),
                        color: Colors.red,
                      ),
                      SizedBox(
                        width: 20,
                      ),

                      //TOMBOL LAWAN

                      RaisedButton(
                        onPressed: () {
                          setState(() {
                            score_lawan++;
                          });
                        },
                        child: Icon(Icons.add),
                        color: Colors.green,
                      ),
                      RaisedButton(
                        onPressed: () {
                          setState(() {
                            score_lawan--;
                          });
                        },
                        child: Icon(Icons.remove),
                        color: Colors.red,
                      ),
                      SizedBox(
                        width: 20,
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 100),
                        child: RaisedButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Text(
                            "Kembali",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold),
                          ),
                          color: Colors.blue,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}

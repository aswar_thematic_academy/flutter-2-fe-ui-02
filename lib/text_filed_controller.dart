import 'package:flutter/material.dart';

class TextFormController extends StatefulWidget {
  @override
  _TextFormControllerState createState() => _TextFormControllerState();
}

class _TextFormControllerState extends State<TextFormController> {
  TextEditingController _controller;

  void initState() {
    _controller = TextEditingController();
    _controller.text = "";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("text"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.all(12),
              child: Text(_controller.text),
            ),
            Container(
              margin: EdgeInsets.all(12),
              child: TextField(
                controller: _controller,
                onSubmitted: (String inputText) {
                  setState(() {});
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

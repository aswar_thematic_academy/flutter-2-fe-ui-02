import 'package:flutter/material.dart';
import 'package:latihan1_statetfull_widget/abou.dart';
import 'package:latihan1_statetfull_widget/content_page.dart';
import 'package:latihan1_statetfull_widget/counter_page.dart';
import 'package:latihan1_statetfull_widget/home_page.dart';
import 'package:latihan1_statetfull_widget/text_filed_controller.dart';
import 'package:latihan1_statetfull_widget/text_form.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      //home: CounterPage(),

      initialRoute: '/',
      routes: {
        '/': (context) => HomePage(),
        '/content': (context) => ContentPage(
              judul: "Halaman Kontent",
            ),
        '/about': (context) => AboutPage(
              judul: "Halaman About",
              kata: "Halo apa kabar ini adalah halaman about pertamaku",
            ),
        '/counter': (context) => CounterPage(),
        '/text-form-controller': (context) => TextFormController(),
        '/text-team': (context) => TextTeam(),
      },
    );
  }
}

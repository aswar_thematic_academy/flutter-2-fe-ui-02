import 'package:flutter/material.dart';

class ContentPage extends StatelessWidget {
  ContentPage({this.judul});

  String judul;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("home page"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(this.judul),
            FlatButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text("Kembali"),
              color: Colors.red,
            )
          ],
        ),
      ),
    );
  }
}

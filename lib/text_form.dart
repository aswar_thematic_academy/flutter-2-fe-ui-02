import 'package:flutter/material.dart';
import 'package:latihan1_statetfull_widget/view_score.dart';

class TextTeam extends StatefulWidget {
  @override
  _TextTeamState createState() => _TextTeamState();
}

class _TextTeamState extends State<TextTeam> {
  TextEditingController controllerTeam1 = new TextEditingController();
  TextEditingController controllerTeam2 = new TextEditingController();

  String nTeam = "";
  String nLawan = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Nama Team"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "Masukkan Nama Team ",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30.0),
            ),
            SizedBox(
              height: 35.0,
            ),
            Container(
              margin: EdgeInsets.all(20),
              child: TextField(
                controller: controllerTeam1,
                decoration:
                    InputDecoration(hintText: 'Input nama team Kandang'),
              ),
            ),
            Column(
              children: [
                Container(
                  margin: EdgeInsets.all(20),
                  child: TextField(
                    controller: controllerTeam2,
                    decoration:
                        InputDecoration(hintText: 'Input nama team Lawan'),
                  ),
                ),
              ],
            ),
            Container(
              margin: EdgeInsets.only(top: 10),
              child: RaisedButton(
                onPressed: () {
                  setState(() {
                    nTeam = controllerTeam1.text;
                    nLawan = controllerTeam2.text;
                  });
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ScoreTeam(
                        tim: nTeam,
                        lawan: nLawan,
                      ),
                    ),
                  );
                },
                child: Text(
                  "KIRIM",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
                color: Colors.blue,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30.0),
                ),
              ),
            ),
            Container(
              child: RaisedButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text(
                  "MENU",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
                color: Colors.red,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30.0),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

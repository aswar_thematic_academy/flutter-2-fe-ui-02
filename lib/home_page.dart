import 'package:flutter/material.dart';
import 'package:latihan1_statetfull_widget/content_page.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("home page"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "Menu Tombol",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 40.0),
            ),
            SizedBox(
              height: 40.0,
            ),
            FlatButton(
              onPressed: () {
                Navigator.pushNamed(context, '/content');
              },
              child: Text("Konten"),
              color: Colors.orange,
              padding: EdgeInsets.only(left: 45, right: 45),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(25.0),
              ),
            ),
            FlatButton(
              onPressed: () {
                Navigator.pushNamed(context, '/about');
              },
              child: Text("About"),
              color: Colors.blue,
              padding: EdgeInsets.only(left: 50, right: 50),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(25.0),
              ),
            ),
            FlatButton(
              onPressed: () {
                Navigator.pushNamed(context, '/counter');
              },
              child: Text("Counter Page"),
              color: Colors.green,
              padding: EdgeInsets.only(left: 25, right: 25),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(25.0),
              ),
            ),
            FlatButton(
              onPressed: () {
                Navigator.pushNamed(context, '/text-form-controller');
              },
              child: Text("text controller"),
              color: Colors.purple[200],
              padding: EdgeInsets.only(left: 25, right: 25),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(25.0),
              ),
            ),
            FlatButton(
              onPressed: () {
                Navigator.pushNamed(context, '/text-team');
              },
              child: Text(
                "Papan Score",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              color: Colors.red[200],
              padding: EdgeInsets.only(left: 30, right: 30),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(25.0),
              ),
            )
          ],
        ),
      ),
    );
  }
}
